// Create a factorial function

const factorial = (n) => {

	if(typeof n !== 'number') return undefined;
	if(n < 0) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n-1)
};

// function divCheck(num){
// 	if(num%5 === 0) return "divisible by 5";
// 	if(num%7 === 0) return  "divisible by 7";
// 	if(num%5 === 0 && num%7 === 0 ) return "divisible by 5 and 7"
// 	return "is NOT divisble by 5 or 7"
// }

const divCheck = (num) => {

	if(num === 0) return "divisible by 5 and 7";
	if(num%5 === 0) return "divisible by 5";
	if(num%7 === 0) return  "divisible by 7";
	if(num%5 === 0 && num%7 === 0) return "divisible by 5 and 7";
	return "NOT divisible by 5 or 7"
};

const names = {
	Eren : {
		"name": "Eren Yeager",
		"alias": "Eren",
		"age": 28
	},
	Levi : {

		"name": "Levi Ackerman",
		"alias": "Levi",
		"age": 38
	}
};

module.exports = {

	factorial: factorial,
	divCheck: divCheck,
	names: names
}