const {factorial, divCheck} = require('../src/util.js');
const {expect, assert} = require('chai');


describe('Test Factorials', () => {

	it('Test that 5! is 120', () => {

		const product = factorial(5)
		expect(product).to.equal(120)
	});

	it('Test that 1! is 1', () => {

		const product = factorial(1)
		assert.equal(product,1)
	});
	it('Test that 0! is 1', () => {

		const product = factorial(0)
		assert.equal(product,1)
	});
	it('Test that 4! is 24', () => {

		const product = factorial(4)
		assert.equal(product,24)
	});
	it('Test that 10! is 3628800', () => {

		const product = factorial(10)
		assert.equal(product,3628800)
	});
	it('Test negative factorial is undefined', () => {
		const product = factorial(-1);
		expect (product).to.equal(undefined)
	});

	it('Test a non-numeric value', () => {
		const product = factorial('wew');
		expect (product).to.equal(undefined)
	})

});

describe('Check Divisibility', () => {

	it('Test that 105 is divisible by 5', () => {

		const number = divCheck(105)
		assert.equal(number, 'divisible by 5')
	})
	it('Test that 14 is divisible by 7', () => {

		const number = divCheck(14)
		assert.equal(number, 'divisible by 7')
	})
	it('Test that 0 is divisible by 5 and 7', () => {

		const number = divCheck(0)
		assert.equal(number, 'divisible by 5 and 7')
	})
	it('Test that 22 is NOT divisible by 5 or 7', () => {

		const number = divCheck(22)
		assert.equal(number, 'NOT divisible by 5 or 7')
	})
})